#include <iostream>
#include <string>

int main()
{
	std::string str = "34512fgch";
	std::cout << "Строка: " << str << '\n';
	std::cout << "Размер строки: " << str.length() << '\n';
	std::cout << "Первый символ строки: " << str[0] << '\n';
	std::cout << "Последний символ: " << str[str.length() - 1] << '\n';

	return 0;
}